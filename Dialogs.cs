﻿using System;
using System.Diagnostics;
using System.IO;
using Ssepan.Io.Core;
using Ssepan.Utility.Core;
//using Microsoft.Data.ConnectionUI;//cannot use until Microsoft.VisualStudio.Data.dll is made redistributable by Microsoft
using System.Reflection;
using Gtk;

namespace Ssepan.Application.Core.GtkSharp
{
    public static class Dialogs
    {
        #region Declarations
        public const string FilterSeparator = "|";
        public const string FilterDescription = "{0} Files(s)";
        public const string FilterFormat = "{0} (*.{1})|*.{1}";
        #endregion Declarations

        #region Methods
        /// <summary>
        /// Get path to save data.
        /// Static. Calls Gtk.FileChooserDialog.
        /// </summary>
        /// <param name="fileDialogInfo">ref FileDialogInfo<Window, ResponseType></param>
        /// <param name="errorMessage">ref string</param>
        public static bool GetPathForSave
        (
			ref FileDialogInfo<Window, ResponseType> fileDialogInfo,
			ref string errorMessage
        )
        {
            bool returnValue = default;
			ResponseType response = ResponseType.None;
			try
			{
                if
                (
                    fileDialogInfo.Filename.EndsWith(fileDialogInfo.NewFilename)
                    ||
                    fileDialogInfo.ForceDialog
                )
                {
					FileChooserDialog fileChooserDialog;
					using
					(
						fileChooserDialog =
							new FileChooserDialog
							(
								fileDialogInfo.ForceDialog ? "Save As..." : "Save...",
								fileDialogInfo.Parent,
								FileChooserAction.Save,
								fileDialogInfo.ForceDialog ? Stock.SaveAs : Stock.Save,
								ResponseType.Ok,
								Stock.Cancel,
								ResponseType.Cancel
							)
					)
					{
						//define location of file for settings by prompting user for filename.
						fileChooserDialog.DoOverwriteConfirmation = true;//TODO:fileDialogInfo.?;
						fileChooserDialog.SetFilename(fileDialogInfo.Filename);
						foreach (string filter in fileDialogInfo.Filters.Split(FileDialogInfo<Window, ResponseType>.FILTER_SEPARATOR))
						{
							string[] nameAndPattern = filter.Split(FileDialogInfo<Window, ResponseType>.FILTER_ITEM_SEPARATOR);
							FileFilter fileFilter = new();
							Debug.Assert(fileFilter != null);
							fileFilter.Name = nameAndPattern[0];
							fileFilter.AddPattern(nameAndPattern[1]);
							fileChooserDialog.AddFilter(fileFilter);
						}
						Debug.Assert(fileChooserDialog.Filters.Length > 0);
						fileChooserDialog.Filter = fileChooserDialog.Filters[0];
						fileChooserDialog.SetCurrentFolder
						(
							fileDialogInfo.InitialDirectory == default
							?
							fileDialogInfo.CustomInitialDirectory
							:
							Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()
						);
						fileChooserDialog.Modal = fileDialogInfo.Modal;

						response = (ResponseType)fileChooserDialog.Run();

						if (response != ResponseType.None)
						{
							if (response == ResponseType.Ok)
							{
								if (string.Equals(Path.GetFileName(fileChooserDialog.Filename), fileDialogInfo.NewFilename, StringComparison.CurrentCultureIgnoreCase))
								{
									//user did not select or enter a name different than new; for now I have chosen not to allow that name to be used for a file.--SJS, 12/16/2005
									string messageTemp = string.Format("The name \"{0}.{1}\" is not allowed; please choose another. Settings not saved.", fileDialogInfo.NewFilename.ToLower(), fileDialogInfo.Extension.ToLower());
									MessageDialogInfo<Window, ResponseType, DialogFlags, MessageType, ButtonsType> messageDialogInfo =
										new (
											parent: fileDialogInfo.Parent,
											modal: fileDialogInfo.Modal,
											title: fileDialogInfo.Title,
											dialogFlags: DialogFlags.DestroyWithParent,
											messageType: MessageType.Info,
											buttonsType: ButtonsType.Ok,
											message: messageTemp,
											response: ResponseType.None
										);

									ShowMessageDialog
									(
										ref messageDialogInfo,
										ref errorMessage
									);
									//Forced cancel
									fileDialogInfo.Response = ResponseType.Cancel;
								}
								else
								{
									//set new filename
									fileDialogInfo.Filename = fileChooserDialog.Filename;
									fileDialogInfo.Filenames = fileChooserDialog.Filenames;
								}
							}
							fileDialogInfo.Response = response;
							returnValue = true;
						}
					}
				}
                else
                {
                    fileDialogInfo.Response = ResponseType.Ok;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }

            fileDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Get path to load data.
        /// Static. Calls Gtk.FileChooserDialog.
        /// </summary>
        /// <param name="fileDialogInfo">ref FileDialogInfo<Window, ResponseType></param>
        /// <param name="errorMessage">ref string</param>
        /// <param name="forceNew">bool</param>
        public static bool GetPathForLoad
        (
            ref FileDialogInfo<Window, ResponseType> fileDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			ResponseType response = ResponseType.None;

			try
            {
                if (fileDialogInfo.ForceNew)
                {
                    fileDialogInfo.Filename = fileDialogInfo.NewFilename;

                    returnValue = true;
                }
                else
                {
					FileChooserDialog fileChooserDialog;
					using
					(
						fileChooserDialog =
							new FileChooserDialog
							(
								fileDialogInfo.Title,
								fileDialogInfo.Parent,
								FileChooserAction.Open,
								Stock.Open,
								ResponseType.Ok,
								Stock.Cancel,
								ResponseType.Cancel
							)
					)
					{
						//define location of file for settings by prompting user for filename.
						// fileChooserDialog.File.Exists
						foreach (string filter in fileDialogInfo.Filters.Split(FileDialogInfo<Window, ResponseType>.FILTER_SEPARATOR))
						{
							string[] nameAndPattern = filter.Split(FileDialogInfo<Window, ResponseType>.FILTER_ITEM_SEPARATOR);
							FileFilter fileFilter = new();
							Debug.Assert(fileFilter != null);
							fileFilter.Name = nameAndPattern[0];
							fileFilter.AddPattern(nameAndPattern[1]);
							fileChooserDialog.AddFilter(fileFilter);
						}
						fileChooserDialog.SelectMultiple = fileDialogInfo.Multiselect;
						fileChooserDialog.SetFilename(fileDialogInfo.Filename);
						fileChooserDialog.SetCurrentFolder(fileDialogInfo.InitialDirectory == default ? fileDialogInfo.CustomInitialDirectory : Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator());
						fileChooserDialog.Modal = fileDialogInfo.Modal;

						response = (ResponseType)fileChooserDialog.Run();

						if (response != ResponseType.None)
						{
							if (response == ResponseType.Ok)
							{
								fileDialogInfo.Filename = fileChooserDialog.Filename;
								fileDialogInfo.Filenames = fileChooserDialog.Filenames;
							}
							fileDialogInfo.Response = response;
							returnValue = true;
						}
					}

					fileChooserDialog?.Destroy();
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }

            fileDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Select a folder path.
        /// Static. Calls Gtk.FileChooserDialog.
        /// </summary>
        /// <param name="fileDialogInfo">ref FileDialogInfo<Window, ResponseType>. returns path in Filename property</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetFolderPath
        (
            ref FileDialogInfo<Window, ResponseType> fileDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			try
			{
				FileChooserDialog fileChooserDialog =
                    new (
                        title: fileDialogInfo.Title,
                        parent: fileDialogInfo.Parent,
                        action: FileChooserAction.SelectFolder,//fileDialogInfo.SelectFolders
                        button_data: [
                            Stock.Open,
                            ResponseType.Ok,
                            Stock.Cancel,
                            ResponseType.Cancel
                        ]
                    )
                    {
                        Modal = fileDialogInfo.Modal
                    };
				fileChooserDialog.SetFilename(string.IsNullOrEmpty(fileDialogInfo.Filename) ? Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator() : fileDialogInfo.Filename);
				ResponseType response = (ResponseType)fileChooserDialog.Run();

				if (response != ResponseType.None)
                {
                    if (response == ResponseType.Ok)
                    {
                        fileDialogInfo.Filename = fileChooserDialog.Filename;
                    }
                    fileDialogInfo.Response = response;
                    returnValue = true;
                }

                fileChooserDialog.Destroy();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            fileDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Shows an About dialog.
        /// Static. Calls Gtk.AboutDialog.
        /// </summary>
        /// <param name="aboutDialogInfo">ref AboutDialogInfo<Window, ResponseType, Gdk.Pixbuf></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool ShowAboutDialog
        (
            ref AboutDialogInfo<Window, ResponseType, Gdk.Pixbuf> aboutDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			AboutDialog aboutDialog = null;

            try
            {
				aboutDialog = new AboutDialog
				{
					Parent = aboutDialogInfo.Parent,
					Modal = aboutDialogInfo.Modal,
					Title = aboutDialogInfo.Title,
					ProgramName = aboutDialogInfo.ProgramName,
					Version = aboutDialogInfo.Version,
					Copyright = aboutDialogInfo.Copyright,
					Comments = aboutDialogInfo.Comments,
					Website = aboutDialogInfo.Website,
					Logo = aboutDialogInfo.Logo
				};

				ResponseType response = (ResponseType)aboutDialog.Run();
				if (response != ResponseType.None)
                {
                    //if (response == ResponseType.Ok)
                    // {

                    // }
                    aboutDialogInfo.Response = response;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            finally
            {
                aboutDialog.Destroy();
            }

            aboutDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Get a printer.
        /// Static. Calls Gtk.PrintUnixDialog.
        /// </summary>
        /// <param name="printerDialogInfo">ref PrinterDialogInfo<Window, ResponseType, Printer>. PrinterDialogInfo</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetPrinter
        (
            ref PrinterDialogInfo<Window, ResponseType, Printer> printerDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			PrintUnixDialog printerDialog = null;

            try
            {
				printerDialog =
					new PrintUnixDialog
					(
						printerDialogInfo.Title,
						printerDialogInfo.Parent
					)
					{
						Modal = printerDialogInfo.Modal
					};

				ResponseType response = (ResponseType)printerDialog.Run();
				if (response != ResponseType.None)
                {
                    if (response == ResponseType.Ok)
                    {
                        printerDialogInfo.Printer = printerDialog.SelectedPrinter;
                        printerDialogInfo.Name = printerDialog.SelectedPrinter.Name;
                    }

                    printerDialogInfo.Response = response;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            finally
            {
                printerDialog.Destroy();
            }

            printerDialogInfo.BoolResult = returnValue;
            return returnValue;
        }
        /// <summary>
        /// Shows a message dialog.
        /// Static. Calls Gtk.MessageDialog.
        /// </summary>
        /// <param name="messageDialogInfo">ref MessageDialogInfo<Window, ResponseType, DialogFlags, MessageType, ButtonsType></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool ShowMessageDialog
        (
            ref MessageDialogInfo<Window, ResponseType, DialogFlags, MessageType, ButtonsType> messageDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			MessageDialog messageDialog = null;

            try
            {
				messageDialog =
					new MessageDialog
					(
						parent_window: messageDialogInfo.Parent,
						flags: messageDialogInfo.DialogFlags,
						type: messageDialogInfo.MessageType,
						bt: messageDialogInfo.ButtonsType,
						format: messageDialogInfo.Message
					)
					{
						Title = messageDialogInfo.Title,
						Modal = messageDialogInfo.Modal
					};

				ResponseType response = (ResponseType)messageDialog.Run();
				if (response != ResponseType.None)
                {
                    //return complex responses in dialoginfo
                    messageDialogInfo.Response = response;
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            finally
            {
                messageDialog.Destroy();
            }

            messageDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Get a color.
        /// Static. Calls Gtk.ColorChooserDialog.
        /// </summary>
        /// <param name="colorDialogInfo">ref ColorDialogInfo<Window, ResponseType, Gdk.RGBA>. ColorDialogInfo</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetColor
        (
            ref ColorDialogInfo<Window, ResponseType, Gdk.RGBA> colorDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			Gdk.RGBA localRGBA;

            try
            {
                colorDialogInfo.Color = Gdk.RGBA.Zero;
				ColorChooserDialog colorChooserDialog =
                    new (
                        title: colorDialogInfo.Title,
                        parent: colorDialogInfo.Parent
                    )
                    {
                        Modal = colorDialogInfo.Modal
                    };

				ResponseType response = (ResponseType)colorChooserDialog.Run();
				if (response != ResponseType.None)
                {
                    if (response == ResponseType.Ok)
                    {
                        localRGBA = colorDialogInfo.Color;
                        localRGBA.Red = colorChooserDialog.Rgba.Red;
                        localRGBA.Green = colorChooserDialog.Rgba.Green;
                        localRGBA.Blue = colorChooserDialog.Rgba.Blue;
                        colorDialogInfo.Color = localRGBA;
                        // Console.WriteLine
                        // (
                        //     string.Format
                        //     (
                        //         "Red:{0},Green:{1},Blue:{2}", 
                        //         colorDialogInfo.Color.Red.ToString(),
                        //         colorDialogInfo.Color.Green.ToString(),
                        //         colorDialogInfo.Color.Blue.ToString()
                        //     )
                        // );
                        // Console.WriteLine
                        // (
                        //     string.Format
                        //     (
                        //         "Red:{0},Green:{1},Blue:{2}", 
                        //         colorChooserDialog.Rgba.Red.ToString(),
                        //         colorChooserDialog.Rgba.Green.ToString(),
                        //         colorChooserDialog.Rgba.Blue.ToString()
                        //     )
                        // );
                    }
                    colorDialogInfo.Response = response;
                    returnValue = true;
                }

                colorChooserDialog.Destroy();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.WriteLine(ex.Message);
            }

            colorDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Get a font descriptor.
        /// Static. Calls Gtk.FontChooserDialog.
        /// </summary>
        /// <param name="fontDialogInfo">ref FontDialogInfo<Window, ResponseType, Pango.FontDescription></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetFont
        (
            ref FontDialogInfo<Window, ResponseType, Pango.FontDescription> fontDialogInfo,//Pango.FontDescription fontDescription,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			try
			{
				// fontResponse = null;
				FontChooserDialog fontChooserDialog =
					new (
						title: fontDialogInfo.Title,
						parent: fontDialogInfo.Parent
					)
					{
						Modal = fontDialogInfo.Modal
					};

				ResponseType response = (ResponseType)fontChooserDialog.Run();
				if (response != ResponseType.None)
                {
                    if (response == ResponseType.Ok)
                    {
                        fontDialogInfo.FontDescription = fontChooserDialog.FontDesc;
                    }
                    fontDialogInfo.Response = response;
                    returnValue = true;
                }//closing dialog window treated as Cancel

                fontChooserDialog.Destroy();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.WriteLine(ex.Message);
            }

            fontDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Perform input of connection string and provider name.
        /// Uses MS Data Connections Dialog.
        /// Note: relies on MS-LPL license and code from http://archive.msdn.microsoft.com/Connection
        /// </summary>
        /// <param name="connectionString">ref string</param>
        /// <param name="providerName">ref string</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetDataConnection
        (
            ref string connectionString,
            ref string providerName,
            ref string errorMessage
        )
        {
            bool returnValue = default;
            //DataConnectionDialog dataConnectionDialog = default(DataConnectionDialog);
            //DataConnectionConfiguration dataConnectionConfiguration = default(DataConnectionConfiguration);

            try
            {
                //dataConnectionDialog = new DataConnectionDialog();

                //DataSource.AddStandardDataSources(dataConnectionDialog);

                //dataConnectionDialog.SelectedDataSource = DataSource.SqlDataSource;
                //dataConnectionDialog.SelectedDataProvider = DataProvider.SqlDataProvider;//TODO:use?

                //dataConnectionConfiguration = new DataConnectionConfiguration(null);
                //dataConnectionConfiguration.LoadConfiguration(dataConnectionDialog);

                //(don't) set to current connection string, because it overwrites previous settings, requiring user to click Refresh in Data Connection Dialog.
                //dataConnectionDialog.ConnectionString = connectionString;

                if (true/*DataConnectionDialog.Show(dataConnectionDialog) == DialogResult.OK*/)
                {
                    ////extract connection string
                    //connectionString = dataConnectionDialog.ConnectionString;
                    //providerName = dataConnectionDialog.SelectedDataProvider.ViewName;

                    ////writes provider selection to xml file
                    //dataConnectionConfiguration.SaveConfiguration(dataConnectionDialog);

                    ////save these too
                    //dataConnectionConfiguration.SaveSelectedProvider(dataConnectionDialog.SelectedDataProvider.ToString());
                    //dataConnectionConfiguration.SaveSelectedSource(dataConnectionDialog.SelectedDataSource.ToString());

                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }
        #endregion Methods
    }
}
