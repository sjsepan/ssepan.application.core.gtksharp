# readme.md - README for Ssepan.Application.Core.GtkSharp

## TODO

### Purpose

To encapsulate common application functionality, reduce custom coding needed to start a new project, and provide consistency across projects.

### Usage notes

~...

### History

6.0:
~refactor to newer C# features
~refactor to existing language types
~perform format linting

5.3:
~update to net8.0 (not documented/released, except for project file)

5.2:
~move usings
~implement dlg filters
~trap new filenames
~ use dlg info BoolResult
~use ACTION_ consts in status
~set dlg titles to action not appname
~move progress/status from view to VM
~add addl std menu actions to vm

5.1:
~use new Website property in AssemblyInfoBase, and set in AssemblyInfo for modules that will display About.

5.0:
~Refactor Ssepan.Application.Core.GtkSharp into several libraries. Left classes specific to GtkSharp in Ssepan.Application.Core.GtkSharp.

Steve Sepan
<sjsepan@yahoo.com>
3/7/2024
